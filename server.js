var mongo = require('mongodb').MongoClient,
  client = require('socket.io').listen(8080).sockets;

// Connect to the MongoDB database
mongo.connect('mongodb://127.0.0.1/chat', function(err, db) {
  if (err) throw err;
  // Once connected to client
  client.on('connection', function(socket) {
    // MongoDB 'messages' collection
    var col = db.collection('messages'),
      sendStatus = function(s) {
        socket.emit('status', s);
      };

    // Emit all messages
    col.find().limit(100).sort({_id: 1}).toArray(function(err, res){
      if(err) throw err;
      socket.emit('output', res);
    });

    // On messages to input
    socket.on('input', function(data) {
      var name = data.name,
        message = data.message,
        // Regex pattern for whitespace
        whitespacePattern = /^\s*$/;
      if (whitespacePattern.test(name) || whitespacePattern.test(message)) {
        // Doing server side validation to prevent tampering server-side
        sendStatus('Name and message is required.')
      } else {
        col.insert({ name: name, message: message}, function() {
          // Emit latest message to All clients
          client.emit('output', [data]);
          sendStatus({
            message: 'Message sent',
            clear: true
          });
        });
      }
    });
  });
});